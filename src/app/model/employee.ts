export interface employee {
    Username: string,
    Firstname:string,
    Lastname:string,
    Email:string,
    birthDate:string,
    basicSalary:number,
    Status:string,
    Group:string,
    description:Date
}