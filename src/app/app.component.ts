import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private route: Router
  ) {}

  title = 'mandiri';

  loginPage: boolean = true;
  value: any [] = [];

  @Input()
  loginEvent = new EventEmitter<boolean>;

  @Input()
  logoutEvent = new EventEmitter<boolean>;

  ngOnInit(): void {
    console.log(localStorage.getItem('page'));
    if(localStorage.getItem('page') == 'viewEmployee') {
      this.login(false);
    }
  }

  login($event: boolean) {
    this.loginPage = $event;
  }

  logout($event: boolean) {
    this.loginPage = $event;
  }
}
