import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MasterService } from 'src/app/service/master.service';

@Component({
  selector: 'app-detailemployee',
  templateUrl: './detailemployee.component.html',
  styleUrls: ['./detailemployee.component.scss']
})
export class DetailemployeeComponent implements OnInit {

  inputdata: any;
  employeedata: any;
  doneLoadOfficeList: Promise<boolean> | undefined;

  nama: string = '';
  email: string = '';
  salary: number = 0;
  status: string = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private ref: MatDialogRef<DetailemployeeComponent>,
    private service: MasterService,
  ) {}

  ngOnInit(): void {
    this.getbyUsername();    
  }

  async getbyUsername() {
    this.inputdata = this.data;
    if(this.inputdata.Username != 'Username') {
      this.service.GetEmployeebyUsername(this.inputdata.Username).subscribe(item => {
      
        this.employeedata = item;
        this.nama = this.employeedata[0].Firstname;
        this.email = this.employeedata[0].Email;
        this.salary = this.employeedata[0].basicSalary;
        this.status = this.employeedata[0].Status;
        this.doneLoadOfficeList = Promise.resolve(true);
      })
    }
  }

  closepopup(){
    this.ref.close('closing from detail');
  }

  

}
