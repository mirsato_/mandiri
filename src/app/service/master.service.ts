import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(
    private http: HttpClient
  ) { }

  getEmployee():Observable<employee[]> {
    return this.http.get<employee[]>("http://localhost:3000/employee")
  }

  GetEmployeebyUsername(Username:any){
    return this.http.get("http://localhost:3000/employee?Username="+Username);
  }

  saveEmployee(data:any){
    return this.http.post("http://localhost:3000/employee",data);
  }
}
