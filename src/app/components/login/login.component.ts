import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output()
  loginEvent = new EventEmitter<boolean>;
  
  signupUsers: any [] = [];
  signupObj: any = {
    userName: '',
    email: '',
    password: ''
  }

  loginObj: any = {
    userName: '',
    password: ''
  }

  loginPage: boolean = true;

  constructor(
    private router: Router
  ) {}
  
  ngOnInit(): void {
    const localData = localStorage.getItem('signUpUsers');
    if(localData != null) {
      this.signupUsers = JSON.parse(localData);
    }
  }

  onSignUp() {
    this.signupUsers.push(this.signupObj);
    localStorage.setItem('signUpUsers', JSON.stringify(this.signupUsers));
    this.signupObj = {
      userName: '',
      email: '',
      password: ''
    }
    return alert('User Created Successfully');
  }

  onLogin() {
    const isUserExist = this.signupUsers.find( x => x.userName == this.loginObj.userName && x.password == this.loginObj.password);
    
    if(isUserExist != undefined ) {
      alert('User Login Successfully');
      this.loginPage = false;
      this.loginEvent.emit(this.loginPage);
    }else {
      this.loginPage = true;
      alert('Wrong Credential');
    }
  }

}
