import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { employee } from 'src/app/model/employee';
import { MasterService } from 'src/app/service/master.service';
import { ModalComponent } from '../modal/modal.component';
import { DetailemployeeComponent } from '../detailemployee/detailemployee.component';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.scss']
})
export class ViewEmployeeComponent implements OnInit {

  @Output()
  loginEvent = new EventEmitter<boolean>;

  @Output()
  logoutEvent = new EventEmitter<boolean>;

  employeeList !: employee [];
  dataSource: any;
  displayedColumns: string[] = ["Username", "Firstname", "Lastname", "Email", "Birth Date", "basicSalary", "Status", "Group", "Description"];
  @ViewChild(MatPaginator) paginator !: MatPaginator;
  @ViewChild(MatSort) sort !: MatSort;
  
  constructor(
    private router: Router,
    private service: MasterService,
    private dialog: MatDialog,
  ) {
  }

  loadEmployee() {
    this.service.getEmployee().subscribe( resp => {
      this.employeeList = resp;
      this.dataSource = new MatTableDataSource<employee>(this.employeeList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  ngOnInit(): void {
    this.loadEmployee();
    this.loginEvent.emit(false);
    localStorage.setItem('page', 'viewEmployee');
  }

  Filterchange(data: Event) {
    const value = (data.target as HTMLInputElement).value;
    this.dataSource.filter = value;
  }

  editEmployee(Username: any) {
    this.Openpopup(Username, 'Edit Employee', ModalComponent);
  }

  addEmployee() {
    this.Openpopup('Username', 'Add Employee', ModalComponent);
  }

  detailEmployee(Username: any) {
    this.Openpopup(Username, 'Detail Employee', DetailemployeeComponent);
  }

  Openpopup(Username: any, title: any, component:any) {
    var _popup = this.dialog.open(component,{
      width: '40%',
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '1000ms',
      height: '400px',
      data: {
        title: title,
        Username: Username
      }
    });

    _popup.afterClosed().subscribe(item =>{
      this.loadEmployee();
    })
  }

  logout() {
    alert('Logging Out');
    localStorage.setItem('page', '');
    this.logoutEvent.emit(true);
  }
  
}
