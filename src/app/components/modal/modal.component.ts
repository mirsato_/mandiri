import { Component, Inject, OnInit, numberAttribute } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MasterService } from 'src/app/service/master.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  inputdata: any;
  editdata: any;

  nama: string = '';
  email: string = '';
  salary: number = 0;
  firstname: string = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private ref: MatDialogRef<ModalComponent>,
    private buildr: FormBuilder,
    private service: MasterService,
  ) {

  }
  ngOnInit(): void {
    this.inputdata = this.data;
    if(this.inputdata.Username != 'Username'){
      this.setpopupdata(this.inputdata.Username);
    }
  }

  setpopupdata(Username: any) {
    this.service.GetEmployeebyUsername(Username).subscribe(item => {
      this.editdata = item;
      this.nama = this.editdata[0].Firstname;
      this.email = this.editdata[0].Email;
      this.firstname = this.editdata[0].Firstname;
      this.salary = this.editdata[0].basicSalary;
      this.myform.setValue({
        Username:this.nama,
        Email:this.email,
        Firstname:this.firstname,
        basicSalary:this.salary
      })
    });
  }

  myform = this.buildr.group({
    Username: this.buildr.control(''),
    Firstname: this.buildr.control(''),
    Email: this.buildr.control(''),
    basicSalary: this.buildr.control(0)
  });

  Saveuser() {
    const arr = [];

    var text = ''
    if(this.myform.get('Username')?.value == '') {
      text = 'Username must be filled';
    }else if(this.myform.get('Firstname')?.value == '') {
      text = 'Firstname must be filled';
    }else if(this.myform.get('Email')?.value == '') {
      text = 'Email must be filled';
    }else if(this.myform.get('basicSalary')?.value == 0 && this.myform.get('basicSalary')?.value == null) {
      text = 'Please Input Number and Not less than Zero';
    }else {
      this.service.saveEmployee(this.myform.value).subscribe(res => {

      });
      text = 'Success Add Data!!';
      this.closepopup();
    }

    arr.push(alert(text));
    
  }

  closepopup() {
    this.ref.close('Closed using function');
  }

  

}
